package com.atlassian.jira.toolkit.customfield;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUser;

import com.atlassian.jira.user.util.UserManager;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.dynamic.P;

import junit.framework.TestCase;
import org.mockito.Mockito;

public class TestParticipantsCFType extends TestCase
{
    private ApplicationUser alice = new MockApplicationUser("alice");
    private Mock mockCommentManager;
    private MockUserConverter mockUserConverter;
    private UserManager mockUserManager;

    protected void setUp() throws Exception
    {
        super.setUp();
        mockUserManager = Mockito.mock(UserManager.class);
        Mockito.when(mockUserManager.getUserByKey(alice.getKey())).thenReturn(alice);
        mockCommentManager = new Mock(CommentManager.class);
        mockUserConverter = new MockUserConverter();
        mockCommentManager.setStrict(true);
        new MockComponentWorker()
                .addMock(UserManager.class, mockUserManager)
                .init();
    }

    public void testGetValueFromIssue() throws Exception
    {
        final JiraAuthenticationContext authenticationContext = createAliceAuthContext();
        // Set up comments with repeated author
        final List<Comment> comments = Arrays.<Comment>asList(
                new MockComment("Dude!", "barney"),
                new MockComment("Awesome", "charlie"),
                new MockComment("Bitchin'", "barney"));
        mockCommentManager.expectAndReturn("getCommentsForUser", P.ANY_ARGS, comments);
        ParticipantsCFType participantsCFType = new ParticipantsCFType(mockUserConverter, authenticationContext, (CommentManager) mockCommentManager.proxy());

        List<ApplicationUser> participants = (List<ApplicationUser>) participantsCFType.getValueFromIssue(null, createMockIssue());

        assertEquals(4, participants.size());
        // assignee
        assertTrue(participants.contains(alice));
        // reporter
        assertTrue(participants.contains(new MockApplicationUser("zed")));
        // commenters
        assertTrue(participants.contains(new MockApplicationUser("barney")));
        assertTrue(participants.contains(new MockApplicationUser("charlie")));
    }

    public void testGetValueFromIssueNullCommentAuthor()
    {
        final JiraAuthenticationContext authenticationContext = createAliceAuthContext();
        // Set up comments with repeated author
        final List<Comment> comments = Arrays.<Comment>asList(
                new MockComment("Dude!", "barney"),
                new MockComment("Awesome", "charlie"),
                new MockComment("Bitchin'", null));
        mockCommentManager.expectAndReturn("getCommentsForUser", P.ANY_ARGS, comments);
        ParticipantsCFType participantsCFType = new ParticipantsCFType(mockUserConverter, authenticationContext, (CommentManager) mockCommentManager.proxy());

        List<ApplicationUser> participants = (List<ApplicationUser>) participantsCFType.getValueFromIssue(null, createMockIssue());

        assertEquals(4, participants.size());
        // assignee
        assertTrue(participants.contains(alice));
        // reporter
        assertTrue(participants.contains(new MockApplicationUser("zed")));
        // commenters
        assertTrue(participants.contains(new MockApplicationUser("barney")));
        assertTrue(participants.contains(new MockApplicationUser("charlie")));
    }

    public void testGetValueFromIssueWithExceptions()
    {
        // Set up comments with exception thrown
        mockCommentManager.expectAndThrow("getCommentsForUser", P.ANY_ARGS, new RuntimeException());
        ParticipantsCFType participantsCFType = new ParticipantsCFType(mockUserConverter, createAliceAuthContext(), (CommentManager) mockCommentManager.proxy());

        List<ApplicationUser> participants = (List<ApplicationUser>) participantsCFType.getValueFromIssue(null, createMockIssue());

        assertEquals(2, participants.size());
        // assignee
        assertTrue(participants.contains(alice));
        // reporter
        assertTrue(participants.contains(new MockApplicationUser("zed")));
        // commenters are skipped
    }

    private JiraAuthenticationContext createAliceAuthContext()
    {
        Mock mockAuthenticationContext = new Mock(JiraAuthenticationContext.class);
        mockAuthenticationContext.setStrict(true);

        mockAuthenticationContext.expectAndReturn("getUser", alice);
        return (JiraAuthenticationContext) mockAuthenticationContext.proxy();
    }

    private Issue createMockIssue()
    {
        final MockIssue mockIssue = new MockIssue(12);
        final ApplicationUser zed = new MockApplicationUser("zed");
        mockIssue.setAssignee(alice.getDirectoryUser());
        mockIssue.setAssigneeId(alice.getKey());
        mockIssue.setReporter(zed.getDirectoryUser());
        mockIssue.setReporterId(zed.getKey());
        mockUserConverter.registerUser(zed);
        mockUserConverter.registerUser(alice);
        return mockIssue;
    }

    private class MockUserConverter implements UserConverter
    {

        @Override
        public String getString(User user)
        {
            return user.getName();
        }

        @Override
        public String getHttpParameterValue(ApplicationUser applicationUser)
        {
            return applicationUser.getUsername();
        }

        @Override
        public String getDbString(ApplicationUser applicationUser)
        {
            return applicationUser.getKey();
        }

        private final Map<String, ApplicationUser> registeredUserKeys = new HashMap<String, ApplicationUser>();
        private final Map<String, ApplicationUser> registeredUserNames = new HashMap<String, ApplicationUser>();
        public void registerUser(ApplicationUser user)
        {
            registeredUserKeys.put(user.getKey(), user);
            registeredUserNames.put(user.getUsername(), user);
        }

        @Override
        public User getUser(String name) throws FieldValidationException
        {
            return ApplicationUsers.toDirectoryUser(registeredUserNames.get(name));
        }

        @Override
        public User getUserEvenWhenUnknown(String s) throws FieldValidationException
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationUser getUserFromHttpParameterWithValidation(String s) throws FieldValidationException
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationUser getUserFromDbString(String key)
        {
            return registeredUserKeys.get(key);
        }

        @Override
        public User getUserObject(String name) throws FieldValidationException
        {
            return getUser(name);
        }
    }

    private class MockComment implements Comment
    {
        final String author;
        public MockComment(final String body, final String author)
        {
            this.author = author;
        }

        @Override
        public String getAuthor()
        {
            return author;
        }

        @Override
        public String getAuthorKey()
        {
            return author;
        }

        @Override
        public User getAuthorUser()
        {
            return new MockUser(author);
        }

        @Override
        public ApplicationUser getAuthorApplicationUser()
        {
            return new MockApplicationUser(author);
        }

        @Override
        public String getAuthorFullName()
        {
            return null;
        }

        @Override
        public String getBody()
        {
            return null;
        }

        @Override
        public Date getCreated()
        {
            return null;
        }

        @Override
        public String getGroupLevel()
        {
            return null;
        }

        @Override
        public Long getId()
        {
            return null;
        }

        @Override
        public Long getRoleLevelId()
        {
            return null;
        }

        @Override
        public ProjectRole getRoleLevel()
        {
            return null;
        }

        @Override
        public Issue getIssue()
        {
            return null;
        }

        @Override
        public String getUpdateAuthor()
        {
            return null;
        }

        @Override
        public User getUpdateAuthorUser()
        {
            return null;
        }

        @Override
        public ApplicationUser getUpdateAuthorApplicationUser()
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getUpdateAuthorFullName()
        {
            return null;
        }

        @Override
        public Date getUpdated()
        {
            return null;
        }
    }
}