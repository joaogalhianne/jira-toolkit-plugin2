package com.atlassian.jira.toolkit.customfield;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldValueProvider;
import com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.converters.SelectConverter;
import com.atlassian.jira.issue.customfields.searchers.AbstractInitializationCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.CustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.SimpleCustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.information.CustomFieldSearcherInformation;
import com.atlassian.jira.issue.customfields.searchers.renderer.CustomFieldRenderer;
import com.atlassian.jira.issue.customfields.searchers.transformer.AbstractCustomFieldSearchInputTransformer;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.customfields.statistics.SelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.searchers.impl.NamedTerminalClauseCollectingVisitor;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.issue.search.searchers.renderer.SearchRenderer;
import com.atlassian.jira.issue.search.searchers.transformer.SearchInputTransformer;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.query.IssueIdClauseQueryFactory;
import com.atlassian.jira.jql.util.JqlIssueKeySupport;
import com.atlassian.jira.jql.util.JqlIssueSupport;
import com.atlassian.jira.jql.validator.ClauseValidator;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.toolkit.RequestAttributeKeys;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.query.Query;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Document;
import webwork.action.Action;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @since v4.0
 */
public class MultiIssueKeySearcher extends AbstractInitializationCustomFieldSearcher implements CustomFieldSearcher, CustomFieldStattable, SortableCustomFieldSearcher
{
    private CustomFieldSearcherInformation searcherInformation;
    private final FieldVisibilityManager fieldVisibilityManager;
    private CustomFieldRenderer searchRenderer;
    private final JqlOperandResolver jqlOperandResolver;
    private final JqlIssueKeySupport jqlIssueKeySupport;
    private final JqlIssueSupport jqlIssueSupport;
    private MultiIssueKeyInputTransformer searchInputTransformer;
    private CustomFieldSearcherClauseHandler customFieldSearcherClauseHandler;
    private final SelectConverter selectConverter;
    private final CustomFieldInputHelper customFieldInputHelper;

    public MultiIssueKeySearcher(final JqlOperandResolver jqlOperandResolver, final JqlIssueKeySupport jqlIssueKeySupport,
            final JqlIssueSupport jqlIssueSupport, final SelectConverter selectConverter,
            final CustomFieldInputHelper customFieldInputHelper, FieldVisibilityManager fieldVisibilityManager)
    {
        this.jqlOperandResolver = jqlOperandResolver;
        this.jqlIssueKeySupport = jqlIssueKeySupport;
        this.jqlIssueSupport = jqlIssueSupport;
        this.selectConverter = selectConverter;
        this.customFieldInputHelper = customFieldInputHelper;
        this.fieldVisibilityManager = fieldVisibilityManager;
    }

    public void init(final CustomField field)
    {
        final ClauseNames names = field.getClauseNames();
        final FieldIndexer indexer = new MultiIssueKeyIndexer(field);
        final CustomFieldValueProvider customFieldValueProvider = new MultiIssueKeyValueProvider();

        this.searcherInformation = new CustomFieldSearcherInformation(field.getId(), field.getNameKey(), Collections.<FieldIndexer>singletonList(indexer), new AtomicReference<CustomField>(field));
        this.searchRenderer = new MultiIssueKeyRenderer(names, getDescriptor(), field, customFieldValueProvider, fieldVisibilityManager);
        this.searchInputTransformer = new MultiIssueKeyInputTransformer(field, field.getId(), names, customFieldInputHelper);

        this.customFieldSearcherClauseHandler = new SimpleCustomFieldSearcherClauseHandler(new MultiIssueKeyClauseValidator(),
                        new IssueIdClauseQueryFactory(jqlOperandResolver, jqlIssueKeySupport, jqlIssueSupport),
                OperatorClasses.EQUALITY_AND_RELATIONAL_WITH_EMPTY, JiraDataTypes.ISSUE);
    }

    public CustomFieldSearcherClauseHandler getCustomFieldSearcherClauseHandler()
    {       
        if (customFieldSearcherClauseHandler == null)
        {
            throw new IllegalStateException("Attempt to retrieve CustomFieldSearcherClauseHandler off uninitialised custom field searcher.");
        }
        return customFieldSearcherClauseHandler;
    }

    public SearcherInformation<CustomField> getSearchInformation()
    {
        if (searcherInformation == null)
        {
            throw new IllegalStateException("Attempt to retrieve SearcherInformation off uninitialised custom field searcher.");
        }
        return searcherInformation;
    }

    public SearchInputTransformer getSearchInputTransformer()
    {
        if (searchInputTransformer == null)
        {
            throw new IllegalStateException("Attempt to retrieve SearchInputTransformer off uninitialised custom field searcher.");
        }
        return searchInputTransformer;
    }

    public SearchRenderer getSearchRenderer()
    {
        if (searchRenderer == null)
        {
            throw new IllegalStateException("Attempt to retrieve SearchRenderer off uninitialised custom field searcher.");
        }
        return searchRenderer;
    }

    public StatisticsMapper getStatisticsMapper(final CustomField customField)
    {
        return new SelectStatisticsMapper(customField, selectConverter, ComponentAccessor.getJiraAuthenticationContext(), customFieldInputHelper);
    }

    public LuceneFieldSorter getSorter(final CustomField customField)
    {
        return new MultiIssueKeyFieldSorter(customField.getId());
    }

    private static class MultiIssueKeyIndexer implements FieldIndexer
    {
        private CustomField field;

        public MultiIssueKeyIndexer(final CustomField field)
        {
            this.field = field;
        }

        public String getId()
        {
            return field.getId();
        }

        public String getDocumentFieldId()
        {
            return field.getId();
        }

        public void addIndex(final Document document, final Issue issue)
        {
            // don't actually do anything here
        }

        public boolean isFieldVisibleAndInScope(final Issue issue)
        {
            return true;
        }
    }

    private class MultiIssueKeyValueProvider implements CustomFieldValueProvider
    {
        public Object getStringValue(final CustomField customField, final FieldValuesHolder fieldValuesHolder)
        {
            CustomFieldParams customFieldParams = customField.getCustomFieldValues(fieldValuesHolder);
            Collection col = customFieldParams.getValuesForNullKey();
            StringBuffer sb = null;
            if (col == null)
            {
                return null;
            }

            for(Iterator i = col.iterator(); i.hasNext();)
            {
                if (sb == null)
                {
                    sb = new StringBuffer((String)i.next());
                } else
                {
                    sb.append(", ").append((String)i.next());
                }
            }

            return (sb == null ? null : sb.toString());
        }

        public Object getValue(final CustomField customField, final FieldValuesHolder fieldValuesHolder)
        {
            return getStringValue(customField, fieldValuesHolder);
        }
    }

    private class MultiIssueKeyInputTransformer extends AbstractCustomFieldSearchInputTransformer implements SearchInputTransformer
    {
        private ClauseNames clauseNames;

        public MultiIssueKeyInputTransformer(CustomField field, String urlParameterName, final ClauseNames clauseNames, final CustomFieldInputHelper customFieldInputHelper)
        {
            super(field, urlParameterName, customFieldInputHelper);
            this.clauseNames = clauseNames;
        }

        @Override
        public boolean doRelevantClausesFitFilterForm(final User searcher, final Query query, final SearchContext searchContext)
        {
            return true;
        }

        @Override
        protected Clause getClauseFromParams(final User searcher, final CustomFieldParams customFieldParams)
        {
            Collection<String> searchValues = customFieldParams.getAllValues();
            if (searchValues.size() == 1)
            {
                final String[] issueKeys = extractIssueKeys(searchValues.iterator().next());
                return new TerminalClauseImpl(getClauseName(searcher, clauseNames), issueKeys);
            }
            return null;
        }

        private String[] extractIssueKeys(final String searchValues)
        {
            return StringUtils.split(searchValues, ", ");
        }

        @Override
        protected CustomFieldParams getParamsFromSearchRequest(final User searcher, final Query query, final SearchContext searchContext)
        {
            final Clause whereClause = query.getWhereClause();
            // You can get a null where clause eg when the JQL has only an order-by clause.
            if (whereClause == null)
            {
                return null;
            }
            NamedTerminalClauseCollectingVisitor visitor = new NamedTerminalClauseCollectingVisitor(clauseNames.getJqlFieldNames());
            whereClause.accept(visitor);

            final List<TerminalClause> namedClauses = visitor.getNamedClauses();
            if (namedClauses.size() == 1) {
                TerminalClause myClause = namedClauses.get(0);
                if (Operator.IN.equals(myClause.getOperator()))
                {
                    final String keys = getCommaSeparatedKeys(myClause.getOperand());
                    final String[] issueKeys = extractIssueKeys(keys);
                    JiraAuthenticationContextImpl.getRequestCache().put(RequestAttributeKeys.MULTIKEY_SEARCHING, Arrays.asList(issueKeys));
                    return new CustomFieldParamsImpl(getCustomField(), keys);
                }
            }
            return null;
        }

        private String getCommaSeparatedKeys(final Operand operand)
        {
            StringBuilder builder = new StringBuilder();
            if (operand instanceof MultiValueOperand)
            {
                MultiValueOperand multiValueOperand = (MultiValueOperand) operand;
                boolean first = true;
                for (Operand child : multiValueOperand.getValues())
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        builder.append(", ");
                    }
                    if (child instanceof SingleValueOperand)
                    {
                        builder.append(((SingleValueOperand)child).getStringValue());
                    }
                    else
                    {
                        // this shouldn't happen but we'll handle it anyway.
                        builder.append(child.getDisplayString());
                    }
                }
                return builder.toString();
            }
            return null;
        }
    }

    private class MultiIssueKeyClauseValidator implements ClauseValidator
    {
        @Override
        public MessageSet validate(final User searcher, final TerminalClause terminalClause)
        {
            return new MessageSetImpl();
        }
    }

    private static class MultiIssueKeyFieldSorter implements LuceneFieldSorter
    {
        private final String customFieldId;

        public MultiIssueKeyFieldSorter(String customFieldId)
        {
            this.customFieldId = customFieldId;
        }

        public String getDocumentConstant()
        {
            return DocumentConstants.ISSUE_KEY;
        }

        public Object getValueFromLuceneField(String documentValue)
        {
            return documentValue;
        }

        public Comparator getComparator()
        {
            return new MultiIssueKeyComparator();
        }

        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }

            final MultiIssueKeyFieldSorter that = (MultiIssueKeyFieldSorter) o;

            return (customFieldId != null ? customFieldId.equals(that.customFieldId) : that.customFieldId == null);
        }

        public int hashCode()
        {
            return (customFieldId != null ? customFieldId.hashCode() : 0);
        }
    }

    private class MultiIssueKeyRenderer extends CustomFieldRenderer
    {
        public MultiIssueKeyRenderer(final ClauseNames clauseNames, final CustomFieldSearcherModuleDescriptor descriptor,
                final CustomField field, final CustomFieldValueProvider customFieldValueProvider, final FieldVisibilityManager fieldVisibilityManager)
        {
            super(clauseNames, descriptor, field, customFieldValueProvider, fieldVisibilityManager);
        }

        @Override
        public String getEditHtml(final User searcher, SearchContext searchContext, FieldValuesHolder fieldValuesHolder, Map<?, ?> displayParameters, Action action)
        {
            return super.getEditHtml(searcher, searchContext, fieldValuesHolder, displayParameters, action);
        }
        @Override
        public String getEditHtml(SearchContext searchContext, FieldValuesHolder fieldValuesHolder, Map<?, ?> displayParameters, Action action, Map<String, Object> velocityParams)
        {
            return super.getEditHtml(searchContext, fieldValuesHolder, displayParameters, action, velocityParams);
        }
    }
}
