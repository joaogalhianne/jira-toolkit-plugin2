package com.atlassian.jira.toolkit.customfield;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.util.UserUtil;

import org.apache.log4j.Logger;

public class LastCommentedByUserCFType extends CalculatedCFType implements SortableCustomField
{
    /**
     * The group whose members are regarded as non-Users.
     */
    private static final String JIRA_DEV = "jira-developers";
    private static final Logger log = Logger.getLogger(LastCommentedByUserCFType.class);

    private final JiraAuthenticationContext authenticationContext;

    private final CommentManager commentManager;
    private final UserUtil userUtil;
    private final GroupManager groupManager;

    public LastCommentedByUserCFType(CommentManager commentManager, JiraAuthenticationContext authenticationContext,
            UserUtil userUtil, GroupManager groupManager)
    {
        this.commentManager = commentManager;
        this.authenticationContext = authenticationContext;
        this.userUtil = userUtil;
        this.groupManager = groupManager;
    }

    public String getStringFromSingularObject(Object value)
    {
        return value != null ? value.toString() : Boolean.FALSE.toString();
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        if (string != null)
        {
            return (string);
        }
        else
        {
            return Boolean.FALSE.toString();
        }
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        Boolean lastCommentedByUser = Boolean.TRUE;
        try
        {
            Comment lastComment = commentManager.getLastComment(issue);
            User lastActionedUser = issue.getReporter();
            if (lastComment != null)
            {
                lastActionedUser = lastComment.getAuthorApplicationUser().getDirectoryUser();
            }

            if (lastActionedUser != null && groupManager.isUserInGroup(lastActionedUser,
                    groupManager.getGroup(JIRA_DEV)))
            {
                lastCommentedByUser = Boolean.FALSE;
            }
        }
        catch (Exception e)
        {
            log.debug(e.getMessage() + " - user most probably used to exist but doesn't now");
        }

        return lastCommentedByUser.toString();
    }
}