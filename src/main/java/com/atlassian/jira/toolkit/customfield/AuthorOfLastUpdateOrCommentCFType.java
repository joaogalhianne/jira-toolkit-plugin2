/**
 *
 */
package com.atlassian.jira.toolkit.customfield;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.comparator.UserComparator;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

import org.apache.log4j.Logger;

/**
 * Get the author of either the last update or the last comment, whatever came latest.
 *
 * @author Paul Curren
 */
public class AuthorOfLastUpdateOrCommentCFType extends CalculatedCFType implements SortableCustomField, UserField
{

    private static final Logger log = Logger.getLogger(AuthorOfLastUpdateOrCommentCFType.class);
    private static final UserComparator USER_COMPARATOR = new UserComparator();

    private final CommentManager commentManager;
    private final UserConverter userConverter;
    private final ChangeHistoryManager changeHistoryManager;
    private final WorklogManager worklogManager;

    public AuthorOfLastUpdateOrCommentCFType(CommentManager commentManager, UserConverter userConverter,
            ChangeHistoryManager changeHistoryManager, WorklogManager worklogManager)
    {
        this.commentManager = commentManager;
        this.userConverter = userConverter;
        this.changeHistoryManager = changeHistoryManager;
        this.worklogManager = worklogManager;
    }

    public Object getSingularObjectFromString(String username) throws FieldValidationException
    {
        return userConverter.getUser(username);
    }

    public String getStringFromSingularObject(Object obj)
    {
        return userConverter.getString(ApplicationUsers.toDirectoryUser((ApplicationUser) obj));
    }

    /**
     * @return a String representing the user name of the last updater or commenter.
     */
    public Object getValueFromIssue(CustomField customfield, Issue issue)
    {
        String updaterKey = issue.getReporterId(); //key
        Date updateTime = issue.getCreated();

        final List changeHistories = changeHistoryManager.getChangeHistoriesForUser(issue, null);
        if (!changeHistories.isEmpty())
        {
            ChangeHistory latestChangeHistory = (ChangeHistory) changeHistories.get(changeHistories.size() - 1);
            updaterKey = latestChangeHistory.getAuthor(); //key
            updateTime = latestChangeHistory.getTimePerformed();
        }

        // we now have the latest change. Check the dates of each comment against the last update time.
        final List comments = commentManager.getComments(issue);
        for (Iterator it = comments.iterator(); it.hasNext();)
        {
            Comment comment = (Comment) it.next();
            if (comment.getCreated().after(updateTime))
            {
                updaterKey = comment.getAuthor(); //key
                updateTime = comment.getCreated();
            }
        }

        //now check that if there's any later worklogs
        final List worklogs = worklogManager.getByIssue(issue);
        if (!worklogs.isEmpty())
        {
            Worklog latestWorklog = (Worklog) worklogs.get(worklogs.size() - 1);
            if (latestWorklog.getUpdated().after(updateTime))
            {
                updaterKey = latestWorklog.getUpdateAuthor(); //key
            }
        }

        try
        {
            return userConverter.getUserFromDbString(updaterKey);
        }
        catch (FieldValidationException e)
        {
            log.warn("Userkey '" + updaterKey +
                     "' could not be found. The user has probably been deleted and was the last user to update issue '"
                     + issue.getKey() + "'.");
        }
        return null;
    }

    public int compare(Object o1, Object o2, FieldConfig fieldConfig)
    {
        if (o1 instanceof User && o2 instanceof User)
        {
            return USER_COMPARATOR.compare(
                    ApplicationUsers.toDirectoryUser((ApplicationUser) o1),
                    ApplicationUsers.toDirectoryUser((ApplicationUser) o2));
        }
        else
        {
            throw new IllegalArgumentException("Object passed must be null or of type User " + o1 + " " + o2);
        }
    }
}
