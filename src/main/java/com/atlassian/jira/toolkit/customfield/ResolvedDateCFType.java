package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.EasyList;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ResolvedDateCFType extends CalculatedCFType implements SortableCustomField, DateField
{
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String TARGET_VALUE = "5";
    private static final String TARGET_FIELD = "status";
    
    private static final Logger log = Logger.getLogger(ResolvedDateCFType.class);

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final JiraAuthenticationContext authenticationContext;


    // ---------------------------------------------------------------------------------------------------- Constructors
    public ResolvedDateCFType(JiraAuthenticationContext authenticationContext)
    {
        this.authenticationContext = authenticationContext;
    }

    // -------------------------------------------------------------------------------------------------- Public Methods

    public String getStringFromSingularObject(Object singularObject)
    {
        if (singularObject != null)
        {
            Timestamp date = (Timestamp) singularObject;
            return authenticationContext.getOutlookDate().format(date);
        }
        else
        {
            return null;
        }
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        if (StringUtils.isNotBlank(string))
        {
            try
            {
                Date date = authenticationContext.getOutlookDate().parseDatePicker(string);
                return new Timestamp(date.getTime());
            }
            catch (ParseException e)
            {
                throw new FieldValidationException(ExceptionUtils.getFullStackTrace(e));
            }
        }
        else
        {
            return null;
        }
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        try
        {
            if (issue != null)
            {
                GenericValue issueGv = issue.getGenericValue();
                if (issueGv != null)
                {
                    List relatedGroups = issueGv.getRelated("ChildChangeGroup", null, EasyList.build("created DESC"));
                    for (Iterator iterator = relatedGroups.iterator(); iterator.hasNext();)
                    {
                        GenericValue changeGroup = (GenericValue) iterator.next();
                        List matchingChangeItems = changeGroup.getRelated("ChildChangeItem",
                                                                           FieldMap.build("newvalue", TARGET_VALUE).add("field", TARGET_FIELD),
                                                                           null);
                        if (matchingChangeItems != null && !matchingChangeItems.isEmpty())
                        {
                            return changeGroup.getTimestamp("created");
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.warn(e, e);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------- Helper Methods


}
