package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.toolkit.RequestAttributeKeys;

import java.util.List;
import java.util.Map;

public class MultiIssueKeyCFType extends CalculatedCFType
{

    public String getStringFromSingularObject(Object singularObject)
    {
        return null;
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        return null;
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        return null;
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem)
    {
        List keys = (List)JiraAuthenticationContextImpl.getRequestCache().get(RequestAttributeKeys.MULTIKEY_SEARCHING);
        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);

        if (keys != null && keys.size() != 0)
        {
            params.put("isBeingUsed", "isBeingUsed");
        }
        return params;
    }
}

