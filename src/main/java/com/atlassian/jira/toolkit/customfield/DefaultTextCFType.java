package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.renderer.wiki.AtlassianWikiRenderer;

import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.config.CoreFeatures.ON_DEMAND;


public class DefaultTextCFType extends GenericTextCFType
{
    private FeatureManager featureManager;
    private RendererManager rendererManager;

    public DefaultTextCFType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager, FeatureManager featureManager, RendererManager rendererManager)
    {
        super(customFieldValuePersister, genericConfigManager);
        this.rendererManager = rendererManager;
        this.featureManager = featureManager;
    }

    @Override
    public String getValueFromIssue(CustomField field, Issue issue)
    {
        FieldConfig config = field.getRelevantConfig(issue);
        if (config == null)
        {
            return null;
        }
        else
        {
            return super.getDefaultValue(config);
        }
    }

    /*
     * Returning null here for the default value because the new quick edit plugin only forces fields to be required
     * on the create screen if they have *no* default value.  On quick edit users will still be able to remove the
     * field but that's kinda the whole point of quick-edit!
     */
    @Override
    public String getDefaultValue(final FieldConfig fieldConfig)
    {
        return null;
    }

    /*
     * Populating the velocity context here with the actual default value (the message) that will be rendered
     * on the view screen.
     */
    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField customField, FieldLayoutItem fieldLayoutItem)
    {
        final Map<String, Object> params = super.getVelocityParameters(issue, customField, fieldLayoutItem);
        if (issue != null)
        {
            String value = null;
            final FieldConfig config = customField.getRelevantConfig(issue);
            if (config != null)
            {
                value = super.getDefaultValue(config);
            }
            if (value != null)
            {
                setVelocityValueParametersValue(params, value);
            }
        }
        return params;
    }

    /**
     * Used to set the the 'value' for the 'value' and 'defaultValue' velocity parameter only
     * @param params a (velocity parameter) map
     * @param value the value to set for the 'value' and 'defaultValue' key in the params map
     */
    protected void setVelocityValueParametersValue(Map<String, Object> params, String value)
    {
        /* if running in ondemand */
        if (featureManager.isEnabled(ON_DEMAND))
        {
            value = rendererManager.getRenderedContent(AtlassianWikiRenderer.RENDERER_TYPE, value, null);
        }

        params.put("value", value);
        params.put("defaultValue", value);

    }

    @Override
    public void createValue(CustomField field, Issue issue, String value)
    {
    }

    @Override
    public void updateValue(CustomField customField, Issue issue, String value)
    {
    }

    @Override
    public String getChangelogValue(CustomField customField, String value)
    {
        return null;
    }

    @Override
    public String getChangelogString(CustomField customField, String value)
    {
        return null;
    }

    /*
     * Need to prevent validation from failing if the field is made required.  So we return an empty string here instead
     * of null so that the CustomFieldImpl thinks that this field has a value.
     */
    @Override
    public String getValueFromCustomFieldParams(final CustomFieldParams relevantParams) throws FieldValidationException
    {
        final Collection values = relevantParams.getValuesForNullKey();
        if (!values.isEmpty())
        {
            return (String) values.iterator().next();
        }
        return "";

    }

}
