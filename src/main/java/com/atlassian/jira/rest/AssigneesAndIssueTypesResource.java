package com.atlassian.jira.rest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.comparator.UserCachingComparator;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.permission.PermissionContext;
import com.atlassian.jira.permission.PermissionContextFactory;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import static com.atlassian.jira.security.Permissions.ASSIGNABLE_USER;
import static com.atlassian.jira.security.Permissions.BROWSE;

import com.atlassian.jira.user.ApplicationUser;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Gets the assignable users and issue types for a project. 
 *
 * @since v4.0
 */
@Path ("/assigneesAndIssueTypes")
@Produces ({ MediaType.APPLICATION_JSON })
public class AssigneesAndIssueTypesResource {

    private static final Logger log = Logger.getLogger(AssigneesAndIssueTypesResource.class);

    public static final CacheControl NO_CACHE = new CacheControl();

    private static final Comparator<User> BEST_NAME_COMPARATOR = new UserCachingComparator();

    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final PermissionSchemeManager permissionSchemeManager;
    private final PermissionContextFactory permissionContextFactory;
    private final ProjectManager projectManager;
    private final IssueTypeSchemeManager issueTypeSchemeManager;

    public AssigneesAndIssueTypesResource(final JiraAuthenticationContext authenticationContext,
            final PermissionManager permissionManager, PermissionSchemeManager permissionSchemeManager,
            PermissionContextFactory permissionContextFactory, ProjectManager projectManager,
            IssueTypeSchemeManager issueTypeSchemeManager) {
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.permissionSchemeManager = permissionSchemeManager;
        this.permissionContextFactory = permissionContextFactory;
        this.projectManager = projectManager;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        log.debug("Constructed.");
    }

    @GET
    public Response generate(@QueryParam ("projectId") String projectId) {
        ApplicationUser user = authenticationContext.getUser();
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        if (StringUtils.isBlank(projectId)) {
            log.debug("No project id");
            errors.add(new ErrorMessage("projectId", "Please choose a project."));
        } else {
            try {
                Long pid = Long.parseLong(projectId);
                final Project project = projectManager.getProjectObj(pid);
                final boolean canSeeProject = permissionManager.hasPermission(BROWSE, project, user);
                if (!canSeeProject) {
                    log.debug("user can't see project");
                    errors.add(new ErrorMessage("projectId", "Project id is wrong"));
                } else {
                    // user has browse permission to existing project
                    final List<User> assigneesList = getAssigneesForProject(project);
                    final Collection<IssueType> issueTypes = getIssueTypesForProject(project);
                    return buildResponse(assigneesList, issueTypes);
                }
            } catch (NumberFormatException e) {
                log.debug("NumberFormatException for projectId: " + projectId);
                errors.add(new ErrorMessage("projectId", "Project id expected to be numeric"));
            } catch (DataAccessException e) {
                log.debug("Incorrect projectId: " + projectId);
                errors.add(new ErrorMessage("projectId", "Project id is wrong"));
            }
        }
        return errorResponse(errors);
    }

    private Collection<IssueType> getIssueTypesForProject(final Project project) {
        return issueTypeSchemeManager.getIssueTypesForProject(project);
    }

    private List<User> getAssigneesForProject(final Project project) {
        final PermissionContext context = permissionContextFactory.getPermissionContext(project);
        final Collection<User> assignables = permissionSchemeManager.getUsers((long) ASSIGNABLE_USER, context);
        final List<User> assigneesList = new ArrayList<User>(assignables);
        Collections.sort(assigneesList, BEST_NAME_COMPARATOR);
        return assigneesList;
    }

    private Response buildResponse(final List<User> assignables, final Collection<IssueType> issueTypes) {
        ArrayList<UserBean> users = new ArrayList<UserBean>();
        for (User user : assignables) {
            users.add(new UserBean(user));
        }
        ArrayList<IssueTypeBean> issueTypeBeans = new ArrayList<IssueTypeBean>();
        for (IssueType issueType : issueTypes) {
            issueTypeBeans.add(new IssueTypeBean(issueType));
        }
        AssigneesAndIssueTypes aait = new AssigneesAndIssueTypes(users, issueTypeBeans);
        if (log.isDebugEnabled()) {
            log.debug("returning " + users.size() + " assignees and " + issueTypeBeans.size() + " issue types");
        }
        return Response.ok(aait).cacheControl(NO_CACHE).build();
    }

    private Response errorResponse(final List<ErrorMessage> errors) {
        return Response.status(400).entity(new ErrorList(errors)).cacheControl(NO_CACHE).build();
    }

    @XmlRootElement
    private static class ErrorList {
        @XmlElement
        private List<ErrorMessage> errors;

        private ErrorList() {
        }

        private ErrorList(final List<ErrorMessage> errors) {
            this.errors = errors;
        }
    }

    @XmlRootElement
    private static class ErrorMessage {
        @XmlElement
        private String field;
        @XmlElement
        private String message;

        private ErrorMessage() {
        }

        public ErrorMessage(final String field, final String message) {
            this.field = field;
            this.message = message;
        }
    }

    @XmlRootElement
    public static class AssigneesAndIssueTypes {
        @XmlElement
        private Collection<UserBean> assignees;
        @XmlElement
        private Collection<IssueTypeBean> issueTypes;

        public AssigneesAndIssueTypes() {
        }

        AssigneesAndIssueTypes(final Collection<UserBean> assignees, final Collection<IssueTypeBean> issueTypes) {
            this.assignees = assignees;
            this.issueTypes = issueTypes;
        }
    }

    @XmlRootElement
    public static class IssueTypeBean {
        @XmlElement
        private String id;
        @XmlElement
        private String name;

        @SuppressWarnings ({ "UnusedDeclaration", "unused" })
        private IssueTypeBean() {
        }

        public IssueTypeBean(final IssueType issueType) {
            this.id = issueType.getId();
            this.name = issueType.getName();
        }
    }

    ///CLOVER:OFF
    @XmlRootElement
    public static class UserBean {
        @XmlElement
        private String username;
        @XmlElement
        private String fullName;
        @XmlElement
        private String email;

        @SuppressWarnings ({ "UnusedDeclaration", "unused" })
        private UserBean() {
        }

        UserBean(User user) {
            this(user.getName(), user.getDisplayName(), user.getEmailAddress());
        }

        UserBean(String username, String fullName, String email) {
            this.username = username;
            this.fullName = fullName;
            this.email = email;
        }
    }
    ///CLOVER:ON
}
